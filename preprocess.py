#!/usr/bin/env python
# coding: utf-8

import re

from konlpy.tag import Mecab
import kss
import numpy as np
import pandas as pd


def drop_row(content):

    dropData = content.dropna(axis=0)

    # Delete interview data
    counter = 0
    index = []
    for n in dropData['content']:
        if n.find('■ 방송') != -1 or n.find('■ 대담') != -1:
            index.append(counter)
        counter += 1
    dropData = dropData.drop(index)
    # Reset row number
    dropData.reset_index(drop=True, inplace=True)

    # Delete one sentence news
    drop_row = []
    for i in range(len(dropData)):
        split = kss.split_sentences(dropData.iloc[i, 4])
        if len(split) == 1:
            drop_row.append(i)
    dropData = dropData.drop(drop_row, axis=0)

    return dropData



class ExtractNouns:
    def __init__(self, content):
        self.content = content
        self.change = self.change_text()
        self.clean_list = self.preprocessing()
        
    def change_text(self):
        content = self.content
        # Change keywords related to covid 19 to '<COVID 19>' at once
        covid_list = ['신종 코로나 바이러스 감염증', '신종코로나바이러스감염증', '신종 코로나바이러스 감염증', '신종 코로나 바이러스',
                     '신종코로나바이러스', '코로나19', '신종 코로나', '코로나 바이러스', '코로나바이러스', '우한폐렴', '우한 폐렴', '코로나']
        for c in covid_list:
            content = content.replace(c, '<COVID19>')

        # Change special type keywords
        content = content.replace('추 장관', '추미애')
        content = content.replace('문 대통령', '문재인')
        content = content.replace('긴급 재난 지원금', '긴급재난지원금')
        content = content.replace('긴급 재난지원금', '긴급재난지원금')
        content = content.replace('N번 방', 'N번방')

        return content


    def preprocessing(self):
        r = re.sub('\(.+?\)', '', self.change)
        r = re.sub('\[.+?\]', '', r)
        # Delete email
        r = re.sub(
            '[a-zA-Z0-9+-_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9가-힣=]+\.*[a-zA-Z0-9가-힣=]*', '', r)
        # Delete special pattern
        r = re.sub('사진=뉴스1', '', r)
        r = re.sub('\\xa0', ' ', r)
        # Delete punctuation
        r = re.sub('[-+#/\?:^$@*※~&ㆍ!』·\\|\(\)\[\]\'\"\’\‘\”\“`…》◇◆■◀▶☞]', ' ', r)
        # Change float type to '<Float>'
        final = re.sub('\d+\.\d+', '<Float>', r)
        #final = change_text(r1)
    
        #list_sentence = kss.split_sentences(final)
        list_sentence = final.split('.')
               
        # Delete reporter name
        list_sentence[0] = re.sub('^[\w+ ].*?=', '', list_sentence[0])
        for num, sentence in enumerate(list_sentence):
            #sentence = re.sub('[\"\'\“\”]', ' ', sentence)
            list_sentence[num] = sentence.strip().replace("  ", " ")
                
        return list_sentence


    def extract_nouns(self):
        mecab = Mecab()

        mecab_list = []

        for t in self.clean_list:
            mecab_list.append(" ".join([nouns for nouns in mecab.nouns(t) if len(nouns) >= 2]))

        mecab_list = [nouns for nouns in mecab_list if nouns]

        # mecab_list: list of str (sentence of nouns)
        return mecab_list