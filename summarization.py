#!/usr/bin/env python
# coding: utf-8

import nltk
import numpy as np
from konlpy.tag import Mecab
from collections import defaultdict

from sklearn.preprocessing import normalize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer

from preprocess import ExtractNouns


class GraphMatrix():
    def __init__(self):
        self.tfidf = TfidfVectorizer()
        self.cnt_vec = CountVectorizer()
        self.graph_sentence = []

    def build_words_graph(self, sentence):
        cnt_vec_mat = normalize(self.cnt_vec.fit_transform(sentence).toarray().astype(float), axis=0)
        vocab = self.cnt_vec.vocabulary_
        return np.dot(cnt_vec_mat.T, cnt_vec_mat), {vocab[word]: word for word in vocab}

    def build_sent_graph(self, sentence):
        tfidf_mat = self.tfidf.fit_transform(sentence).toarray()
        self.graph_sentence = np.dot(tfidf_mat, tfidf_mat.T)
        return self.graph_sentence


class Rank():
    def get_ranks(self, graph, d=0.85):
        A = graph
        matrix_size = A.shape[0]
        # tf-idf
        for id in range(matrix_size):
            A[id, id] = 0
            link_sum = np.sum(A[:, id])
            if link_sum != 0:
                A[:, id] /= link_sum
            A[:, id] *= -d
            A[id, id] = 1

        B = (1-d) * np.ones((matrix_size, 1))
        ranks = np.linalg.solve(A, B)
        return {idx: r[0] for idx, r in enumerate(ranks)}


class TextRank():
    def __init__(self, text):
        # 전처리까지 마친 문장
        self.sentences = ExtractNouns(text).preprocessing()
        # 전처리, 명사 추출 끝난 문장 (list of str type)
        self.nouns = ExtractNouns(text).extract_nouns()

        self.graph_matrix = GraphMatrix()
        self.words_graph, self.idx2word = self.graph_matrix.build_words_graph(
            self.nouns)
        self.sent_graph = self.graph_matrix.build_sent_graph(self.nouns)

        self.rank = Rank()
        self.word_rank_idx = self.rank.get_ranks(self.words_graph)
        self.sorted_word_rank_idx = sorted(
            self.word_rank_idx, key=lambda k: self.word_rank_idx[k], reverse=True)

        self.sent_rank_idx = self.rank.get_ranks(self.sent_graph)
        self.sorted_sent_rank_idx = sorted(
            self.sent_rank_idx, key=lambda k: self.sent_rank_idx[k], reverse=True)

    def keywords(self, word_num=10):
        rank = Rank()
        rank_idx = rank.get_ranks(self.words_graph)  # rank_idx == index : rank
        sorted_rank_idx = sorted(
            rank_idx, key=lambda k: rank_idx[k], reverse=True)

        keywords = {}
        index = []
        for idx in sorted_rank_idx[:word_num]:
            index.append(idx)

        for idx in index:
            keywords[self.idx2word[idx]] = rank_idx[idx]

        return keywords

    def summarize(self, sent_num=1):
        summary = []
        index = []
        for idx in self.sorted_sent_rank_idx[:sent_num]:
            index.append(idx)

        # index.sort()
        for idx in index:
            summary.append(self.sentences[idx])

        return summary